<?php
 include '../connection.php';
 include '../function.php';
 session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
        <link rel="stylesheet" href="../css/style.css">
    </head>

<body>
    <div class="container-fluid">
        <div class="row main-content text-center">
            <div class="col-md-12 col-xs-12 col-sm-12 login_form ">
                <div class="container-fluid">
                    <div class="row">
                        <h2><?php
                           if(isset($_SESSION['msg'])){
                               echo $_SESSION['msg'];
                           }
                        ?></h2>
                    </div>
                    <span class="mainerr" id="otp_err" style="display:none;"><b>Wrong OTP!</b>Check your email then enter correct OTP.</span>
                    <div class="row">
                        <form control="" class="form-group" method="post">
                            <div class="row">
                                <input type="text" name="otp" id="otp" class="form__input"
                                    placeholder="Enter OTP" required>
                            </div>
                            <div class="row mx-auto col-md-8">
                                <input type="submit" name="submit" value="Submit" class="btn">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
     if(isset($_POST['submit'])){
        $otp=validate($_POST['otp']);
        if(isset($_SESSION['otp'])){
            if($_SESSION['otp']==$otp){
                ?>
                  <script>
                       var otp=document.getElementById("otp_err");
                       otp.style.display="none";
                      window.location="set_password.php";
                  </script>
                <?php
            }
            else{
               ?>
               <script>
                   var otp=document.getElementById("otp_err");
                   otp.style.display="block";
               </script>
             <?php
            }
        }
    }
    ?>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js"
        integrity="sha384-W8fXfP3gkOKtndU4JGtKDvXbO53Wy8SZCQHczT5FMiiqmQfUpWbYdTil/SxwZgAN" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.min.js"
        integrity="sha384-skAcpIdS7UcVUC05LJ9Dxay8AXcDYfBJqt1CJ85S/CFujBsIzCIv+l9liuYLaMQ/" crossorigin="anonymous">
    </script>
</body>

</html>