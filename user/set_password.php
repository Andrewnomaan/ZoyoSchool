<?php
 include '../connection.php';
 include '../function.php';
 session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
        <link rel="stylesheet" href="../css/style.css">
    </head>

<body>
    <div class="container-fluid">
        <div class="row main-content text-center">
            <div class="col-md-12 col-xs-12 col-sm-12 login_form ">
                <div class="container-fluid">
                    <div class="row">
                        <h2>Set your new password</h2>
                    </div>
                    <span class="mainerr" id="pass_err" style="display:none;">Password and confirm password is not matching</span>
                    <div class="row">
                        <form control="" class="form-group" method="post" onsubmit="return validate()">
                            <div class="row">
                                <input type="password" name="password" id="password" class="form__input"
                                    placeholder="Password" required>
                            </div>
                            <span class="err1" id="pass_len_err" style="display:none;">Password should contain more than 5 character</span>
                            <div class="row">
                                <input type="password" name="cpassword" id="cpassword" class="form__input"
                                    placeholder="Confirm Password" required>
                            </div>
                            <div class="row mx-auto col-md-8">
                                <input type="submit" name="submit" value="Submit" class="btn">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function validate(){
        let password=document.getElementById('password').value;
        let cpassword=document.getElementById('cpassword').value;
        var pass_err=document.getElementById('pass_err');
        var pass_len_err=document.getElementById('pass_len_err');
        if(password!=cpassword){
             pass_err.style.display="block";
             return false;
        }
        else if(password.length<6){
             pass_len_err.style.display="block";
             return false;
        }
        else{
            pass_err.style.display="none";
            pass_len_err.style.display="none";
            return true;
        }
    }
    </script>
    <?php
      if(isset($_POST['submit'])){
        $password=validate($_POST['password']);
        $res=mysqli_query($conn,"UPDATE users SET password ='$password' where email='$_SESSION[email]'");
        if($res){
            ?>
              <script>
                  window.location="login.php";
              </script>
            <?php
            $_SESSION['msg']="Your password has changed.Please enter your login details.";
        }
    }
    ?>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js"
        integrity="sha384-W8fXfP3gkOKtndU4JGtKDvXbO53Wy8SZCQHczT5FMiiqmQfUpWbYdTil/SxwZgAN" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.min.js"
        integrity="sha384-skAcpIdS7UcVUC05LJ9Dxay8AXcDYfBJqt1CJ85S/CFujBsIzCIv+l9liuYLaMQ/" crossorigin="anonymous">
    </script>
</body>

</html>