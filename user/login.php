<?php
session_start();
  include '../connection.php';
  include '../function.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Login Page</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/style.css">
</head>
<body>
	<!-- Main Content -->
	<div class="container-fluid">
		<div class="row main-content bg-success text-center">
            <div class="col-md-8 col-xs-12 col-sm-12 login_form ">
				<div class="container-fluid">
				<div class="msg_forget" style="color:red;display: none;">
					</div>
					<?php
					   if(isset($_SESSION['msg'])){
						   ?>
						     <script>
								  let msg=document.querySelector('.msg_forget');
									 msg.style.display="block";
									 msg.innerHTML="Your Password has changed.Plz enter your login details."; 
								     setTimeout(() => {
										 msg.style.display="none";
									 }, 5000);
							 </script>
						   <?php
					   }
					?>
					<div class="row">
						<h2>User login</h2>
					</div>
					<span class="mainerr" style="display:none;">Invalid email or password</span>
					<div class="row">
						<form control="" class="form-group" method="post">
							<div class="row">
								<input type="text" name="username" id="username" class="form__input" placeholder="Username" required>
							</div>
							<span class="err1" style="display: none;">Username is required</span>
							<div class="row">
								<!-- <span class="fa fa-lock"></span> -->
								<input type="password" name="password" id="password" class="form__input" placeholder="Password" required>
							</div>
							<span class="err2" style="display: none;">Password is required</span>
							<div class="row">
								<input type="submit" name="submit" value="Log in" class="btn">
							</div>
                                <div class="btn-group" role="group" aria-label="First group">
                                    <a href="login.php?student" type="button" class="btn btn-outline-secondary "
                                        style="background-color:#0084B4;">Student</a>
										<a href="login.php?parent" type="button" class="btn btn-outline-secondary"
                                        style="background-color:#e91e63;">Parent</a>
                                </div>
                          
						</form>
						<div class="bottom">
							<a href="forget_password.php" class="forget">Forget password?</a>
							<a href="" class="front">Front Site</a>
							<a href="../admin/superadminlogin.php" class="user">Admin Login</a>
					</div>
					</div>
				</div>
			</div>
			<div class="col-md-4 text-center company__info">
				<span class="company__logo"><h2><span class="fa fa-android"></span></h2></span>
				<h4 class="company_title"></h4>
			</div>
			
		</div>
	</div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js" integrity="sha384-W8fXfP3gkOKtndU4JGtKDvXbO53Wy8SZCQHczT5FMiiqmQfUpWbYdTil/SxwZgAN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.min.js" integrity="sha384-skAcpIdS7UcVUC05LJ9Dxay8AXcDYfBJqt1CJ85S/CFujBsIzCIv+l9liuYLaMQ/" crossorigin="anonymous"></script>
    <?php
if(isset($_POST['submit'])){
	function check($res){
		if(mysqli_num_rows($res)>0){
			?>
			<script>
				let mainerr=document.querySelector('.mainerr');
				let err1=document.querySelector('.err1');
				let err2=document.querySelector('.err2');
				 mainerr.style.display="none";
				 err1.style.display="none";
				 err2.style.display="none";
		  </script>
			<?php
			return 1;
		 }
		 else{
			 ?>
			   <script>
				   let mainerr=document.querySelector('.mainerr');
				   mainerr.style.display="block";
			   </script>
			 <?php
			 return 0;
		 }
	}
	$res="";
	$username=mysqli_real_escape_string($conn,validate($_POST['username']));
   $password=mysqli_real_escape_string($conn,validate($_POST['password']));
	if(isset($_GET['student'])){
	   $res=mysqli_query($conn,"SELECT * FROM users where email ='$username' AND password='$password' AND role='1'");
	   if(check($res)){
		   ?>
		     <script>
				 window.location="student.php";
			 </script>
		   <?php
	   }
	}
	else if(isset($_GET['parent'])){
	   $res=mysqli_query($conn,"SELECT * FROM users where email ='$username' AND password='$password' AND role='2'");
	   if(check($res)){
		   ?>
		     <script>
				 window.location="parent.php";
			 </script>
		   <?php
	   }
	}
	else{
		?>
		<script>
			let mainerr=document.querySelector('.mainerr');
			mainerr.style.display="block";
		</script>
	  <?php
	}
}
?>
</body>
</html>
