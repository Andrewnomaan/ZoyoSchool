<?php
 include '../connection.php';
 include '../function.php';
 session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
        <link rel="stylesheet" href="../css/style.css">
    </head>

<body>
    <div class="container-fluid">
        <div class="row main-content text-center">
            <div class="col-md-12 col-xs-12 col-sm-12 login_form ">
                <div class="container-fluid">
                    <div class="row">
                        <h2>Email-id</h2>
                    </div>
                    <div class="row">
                        <form control="" class="form-group" method="post">
                            <div class="row">
                                <input type="text" name="username" id="username" class="form__input"
                                    placeholder="Enter your email-id" required>
                            </div>
                            <span class="err1" id="err" style="display:none;"><b>Wrong Email!</b>You have no account</span>
                            <div class="row mx-auto col-md-8">
                                <input type="submit" name="submit" value="Send OTP" class="btn">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
       if(isset($_POST['submit'])){
        $email=mysqli_real_escape_string($conn,validate($_POST['username']));
        $sql="SELECT * FROM users where email='$_POST[username]'";
        $res=mysqli_query($conn,$sql);
        if(mysqli_num_rows($res)>0){
        $_SESSION['email']=$email;
        $otp=rand(1000,9999);
        $_SESSION['otp']=$otp;
        $to=$email;
        $subject="OTP";
        $message="Your otp is:".$otp;
        function smtp_mail($to,$subject,$message){
            // https://accounts.google.com/b/0/DisplayUnlockCaptcha
            require_once("PHPMailerAutoload.php");
            $mail = new PHPMailer();                                     
              $mail->isSMTP(); 
              // $mail->SMTPDebug = 1; 
              $mail->SMTPAuth   = true;       
              $mail->SMTPSecure = 'tls';                                               
              $mail->Host       = 'smtp.gmail.com;'; 
              $mail->Port       = 587;                   
              $mail->isHTML(true);     
              $mail->CharSet='UTF-8';                  
              $mail->Username   = 'andrew.nomaan@gmail.com';                 
              $mail->Password   = 'nommu1914gmail.com';                        
              $mail->setFrom($to);                                        
              $mail->Subject = $subject;
              $mail->Body    = $message;
              $mail->AddAddress($to);
              if( $mail->send()){
                ?>
                <script>
                      var err=document.getElementById('err');
                      err.style.display="none";
                    window.location="otp.php";
                </script>
               <?php
               $_SESSION['msg']="We have sent otp to your email.Please enter that otp here.";
              }
          }
          smtp_mail($to,$subject,$message);
    }
    else{
        ?>
        <script>
            var err=document.getElementById('err');
            err.style.display="block";
        </script>
     <?php 
    }
   }
    ?>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js"
        integrity="sha384-W8fXfP3gkOKtndU4JGtKDvXbO53Wy8SZCQHczT5FMiiqmQfUpWbYdTil/SxwZgAN" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.min.js"
        integrity="sha384-skAcpIdS7UcVUC05LJ9Dxay8AXcDYfBJqt1CJ85S/CFujBsIzCIv+l9liuYLaMQ/" crossorigin="anonymous">
    </script>
</body>

</html>